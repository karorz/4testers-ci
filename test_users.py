from users import User


def test_user_initial_state():
    test_user = User("test@example.com", "Passw0rd")
    assert test_user.email == "test@example.com"
    assert test_user.password == "Passw0rd"
    assert not test_user.logged_in


def test_user_succesful_login():
    test_user = User("test@example.com", "Passw0rd")
    test_user.login("Passw0rd")
    assert test_user.logged_in


def test_user_unsuccesful_login():
    test_user = User("test@example.com", "Passw0rd")
    test_user.login("Wrong_Passw0rd")
    assert not test_user.logged_in


def test_user_logout():
    test_user = User("test@example.com", "Passw0rd")
    test_user.login("Passw0rd")
    test_user.logout()
    assert not test_user.logged_in
